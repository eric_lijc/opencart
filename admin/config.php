<?php
// HTTP
define('HTTP_SERVER', 'http://crm-dev.com/opencart/admin/');
define('HTTP_CATALOG', 'http://crm-dev.com/opencart/');

// HTTPS
define('HTTPS_SERVER', 'http://crm-dev.com/opencart/admin/');
define('HTTPS_CATALOG', 'http://crm-dev.com/opencart/');

// DIR
define('DIR_APPLICATION', '/Users/ericlee/Projects/crm/vtigercrm/opencart/admin/');
define('DIR_SYSTEM', '/Users/ericlee/Projects/crm/vtigercrm/opencart/system/');
define('DIR_DATABASE', '/Users/ericlee/Projects/crm/vtigercrm/opencart/system/database/');
define('DIR_LANGUAGE', '/Users/ericlee/Projects/crm/vtigercrm/opencart/admin/language/');
define('DIR_TEMPLATE', '/Users/ericlee/Projects/crm/vtigercrm/opencart/admin/view/template/');
define('DIR_CONFIG', '/Users/ericlee/Projects/crm/vtigercrm/opencart/system/config/');
define('DIR_IMAGE', '/Users/ericlee/Projects/crm/vtigercrm/opencart/image/');
define('DIR_CACHE', '/Users/ericlee/Projects/crm/vtigercrm/opencart/system/cache/');
define('DIR_DOWNLOAD', '/Users/ericlee/Projects/crm/vtigercrm/opencart/download/');
define('DIR_LOGS', '/Users/ericlee/Projects/crm/vtigercrm/opencart/system/logs/');
define('DIR_CATALOG', '/Users/ericlee/Projects/crm/vtigercrm/opencart/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'opencart');
define('DB_PREFIX', 'oc_');
?>